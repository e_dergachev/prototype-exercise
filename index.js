const { o3 } = require("./protochain");

let objToCheck = o3;
const res = [];

while (objToCheck.prototype !== null) {
  res.push(objToCheck.name);
  objToCheck = objToCheck.prototype;
}

module.exports = res;
